# Changelog

## 4.4.0 - 2025-11-25

### Added

- Installable en tant que package Composer

### Changed

- spip/spip#5460 `style_prive_plugin_mots` sans compilation de SPIP
- Compatible SPIP 5.0.0-dev

### Fixed

- !4816 Ajouter le pipeline `afficher_config_objet` qui manquait sur les pages mots et groupes_mots.

### Removed

- !4819 Un fichier de langue obsolète et non traduit.

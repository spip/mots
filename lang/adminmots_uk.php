<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/adminmots?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_associer' => 'Зв’язати',
	'bouton_dissocier' => 'Від’єднати',
	'bouton_fusionner' => 'Об’єднати',

	// E
	'erreur_admin_mot_action_inconnue' => 'Що ви хочете зробити?',
	'erreur_mot_cle_deja' => 'Неможливо: це той самий ключ.',
	'erreur_selection_id' => 'Виберіть ключ або введіть його ID у поле вводу',

	// I
	'icone_administrer_mot' => 'Розширені дії',

	// L
	'label_associer_objets_mot' => '<b>Пов’язати</b> цей ключ з об’єктами, що мають ключ',
	'label_confirm_fusion' => 'Цю операцію не можна скасувати.<br />
<strong>Поточний ключ #@id_mot@ буде видалено</strong>, а наступні зв’язки будуть перенесені до ключа #@id_mot_new@ :',
	'label_confirm_fusion_check' => 'Підтвердьте злиття ключа #@id_mot@ з ключем #@id_mot_new@',
	'label_dissocier_objets_mot' => '<b>Від’єднати</b> цей ключ від об’єктів із ключем',
	'label_fusionner_mot' => '<b>Об’єднати</b> з ключем',
	'label_mot_1_enfant' => 'Дочірній:',
	'label_mot_nb_enfants' => 'Дочірні:',
	'label_mot_parent' => 'Батько:',

	// P
	'placeholder_id_mot' => 'або #ID_MOT',
	'placeholder_select' => 'Оберіть…',

	// R
	'result_associer_nb' => 'було пов’язано з цим ключем',
	'result_associer_ras' => 'Дії не потрібні: всі об’єкти вже пов’язані з цим ключем',
	'result_cancel_ok' => 'Останню операцію скасовано.',
	'result_dissocier_nb' => 'було від’єднано від цього ключа',
	'result_dissocier_ras' => 'Дії не потрібні: жоден цільовий об’єкт не пов’язаний з цим ключем',
	'result_fusionner_ok' => 'Тепер ви можете видалити цей ключ: всі зв’язки перенесено до іншого ключа @mot@.',

	// T
	'titre_formulaire_administrer_mot' => 'Управління ключем',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/adminmots?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_associer' => 'Ampiarahana',
	'bouton_dissocier' => 'Sarahana',
	'bouton_fusionner' => 'Akambana',

	// E
	'erreur_admin_mot_action_inconnue' => 'Inona no tianao hatao ?',
	'erreur_mot_cle_deja' => 'Tsy azo atao : efa io no teny fototra iasanao.',
	'erreur_selection_id' => 'Misafidiana teny fototra iray na ampidiro ao anaty faritra fanoratana ny ID',

	// I
	'icone_administrer_mot' => 'Fitantanana avo lenta',

	// L
	'label_associer_objets_mot' => '<b>Ampiarahana</b> amin’ireo zavatra efa misy azy ity teny ity',
	'label_confirm_fusion' => 'Tsy azo foanana ity fanovana ity.<br />
<strong>Ity teny fototra #@id_mot@ ity dia hofafana</strong> ary ireto rohy manaraka ireto dia hafindra amin’ny teny  #@id_mot_new@ : ',
	'label_confirm_fusion_check' => 'Mariho ho fanekena ny fanakambanana ny teny  #@id_mot@ amin’ny teny #@id_mot_new@',
	'label_dissocier_objets_mot' => '<b>Sarahana</b> amin’ny zavatra efa misy azy ity teny ity',
	'label_fusionner_mot' => '<b>Akambana</b> amin’ny teny fototra',
	'label_mot_1_enfant' => 'Zanany :',
	'label_mot_nb_enfants' => 'Zanany :',
	'label_mot_parent' => 'Reniny :',

	// P
	'placeholder_id_mot' => 'na #ID_MOT',
	'placeholder_select' => 'Safidiana...',

	// R
	'result_associer_nb' => 'dia nampiarahana amin’ity teny fototra ity',
	'result_associer_ras' => 'Tsy misy azo atao : efa manana ity teny fototra ity avokoa ny zavatra rehetra',
	'result_cancel_ok' => 'Nofoanana ny fanovana farany.',
	'result_dissocier_nb' => 'dia nosarahana tamin’ity teny fototra ity',
	'result_dissocier_ras' => 'Tsy misy azo atao : tsy misy zavatra miaraka amin’ity teny fototra ity',
	'result_fusionner_ok' => 'Efa azonao fafana ity teny ity : voafindra amin’ny @mot@ ny rohy rehetra.',

	// T
	'titre_formulaire_administrer_mot' => 'Mitantana ny teny',
];
